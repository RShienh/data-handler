package com.shienh.datahandler.Activities.Calendars;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.button.MaterialButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.shienh.datahandler.Models.Events;
import com.shienh.datahandler.R;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Objects;

import es.dmoral.toasty.Toasty;

public class NewEventActivity extends AppCompatActivity {

    private TextView mNewEventTitle;
    private MaterialButton mNewEventDate, mSave;
    private TextInputEditText mNewEventDetails;
    private String collectionRef = "", mEventTitle = "", mEventDate = "", success = "", issue = "", mEventDetails = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_event);

        //check if the add new event is from which collection. collection reference has been sent along the intent. Use it while saving data
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            collectionRef = bundle.getString("Reference");
            mEventTitle = bundle.getString("place");
        }else{
            Toasty.error(getApplicationContext(),"No Collection Reference",Toasty.LENGTH_LONG).show();
            finish();
        }

        mNewEventTitle = findViewById(R.id.xNewEventTitle);
        mNewEventDate = findViewById(R.id.xNewEventDate);
        mNewEventDetails = findViewById(R.id.xNewEventDetails);
        mSave = findViewById(R.id.xNewEventSave);

        if(!mEventTitle.isEmpty()){
            mNewEventTitle.setText(String.format("%s %s", getString(R.string.adding_new_event_at_jobstart), mEventTitle));
        }

        mNewEventDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int dayOfMonth = c.get(Calendar.DAY_OF_MONTH);
                c.set(2019, 0, 1);
                DatePickerDialog datePickerDialog = new DatePickerDialog(NewEventActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Calendar c = Calendar.getInstance();
                        c.set(Calendar.YEAR, year);
                        c.set(Calendar.MONTH, month);
                        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        mEventDate = DateFormat.getDateInstance(DateFormat.DEFAULT).format(c.getTime());
                        mNewEventDate.setText(mEventDate);
                    }
                }, year, month, dayOfMonth);
                datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
                datePickerDialog.show();
            }
        });

        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveJobPosting();
                if (!issue.isEmpty()) {
                    Toasty.error(getApplicationContext(), issue, Toasty.LENGTH_LONG).show();
                } else {
                    Toasty.success(getApplicationContext(), success, Toasty.LENGTH_LONG).show();
                }
            }
        });


    }

    private void saveJobPosting() {
        mEventDetails = Objects.requireNonNull(mNewEventDetails.getText()).toString().trim();
        if (mEventDate.trim().isEmpty()) {
            issue = "No date for event has been selected";
        } else if (mEventDetails.isEmpty()) {
            issue = "No Event details has been added";
        } else {
            CollectionReference eventRef = FirebaseFirestore.getInstance().collection(collectionRef);
            eventRef.add(new Events(mEventDate, mEventDetails));
            success = "Event has been added";
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            }, 2000);
        }
    }
}
