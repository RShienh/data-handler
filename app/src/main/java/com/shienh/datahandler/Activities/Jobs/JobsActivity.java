package com.shienh.datahandler.Activities.Jobs;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.gson.Gson;
import com.shienh.datahandler.Adapters.JobsAdapter;
import com.shienh.datahandler.Models.Jobs;
import com.shienh.datahandler.R;

public class JobsActivity extends AppCompatActivity {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference jobsRef = db.collection("Jobs");

    private JobsAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jobs);

        setupRecyclerView();

        FloatingActionButton mAddNewJob = findViewById(R.id.xAdd);
        mAddNewJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(JobsActivity.this, NewJobActivity.class));
            }
        });


    }


    private void setupRecyclerView() {
        Query q = jobsRef.orderBy("postDate", Query.Direction.DESCENDING);

        FirestoreRecyclerOptions<Jobs> options = new FirestoreRecyclerOptions.Builder<Jobs>().setQuery(q, Jobs.class).build();

        mAdapter = new JobsAdapter(options);

        RecyclerView recyclerView = findViewById(R.id.xJobsRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);


        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull final RecyclerView.ViewHolder viewHolder, int i) {
                AlertDialog alertDialog = new AlertDialog.Builder(JobsActivity.this).create();
                alertDialog.setTitle("Delete?");
                alertDialog.setMessage("Do you want to delete this?");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAdapter.deleteItem(viewHolder.getAdapterPosition());
                        recreate();
                    }
                });

                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        recreate();
                    }
                });
                alertDialog.show();

            }
        }).attachToRecyclerView(recyclerView);


        mAdapter.setOnItemClickListener(new JobsAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(DocumentSnapshot documentSnapshot, int position) {
                Jobs jobs = documentSnapshot.toObject(Jobs.class);
                Intent i = new Intent(getApplicationContext(), ViewJobActivity.class);
                Gson gson = new Gson();
                String myJson = gson.toJson(jobs);
                i.putExtra("job", myJson);
                startActivity(i);

            }
        });

    }


    @Override
    protected void onStart() {
        super.onStart();
        mAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAdapter.stopListening();
    }
}
