package com.shienh.datahandler.Activities.Jobs;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.widget.TextView;

import com.google.gson.Gson;
import com.shienh.datahandler.Models.Jobs;
import com.shienh.datahandler.R;
import com.shienh.datahandler.Views.PerfectScrollableTextView;

public class ViewJobActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_job);

        Gson gson = new Gson();
        Jobs j = gson.fromJson(getIntent().getStringExtra("job"), Jobs.class);

        TextView mGetJobID = findViewById(R.id.xGetJobID);
        TextView mGetJobTitle = findViewById(R.id.xGetJobTitle);
        TextView mGetPostedDate = findViewById(R.id.xGetJobPost);
        TextView mGetApplyDate = findViewById(R.id.xGetJobEnd);
        TextView mGetStatus = findViewById(R.id.xGetJobStatus);
        TextView mGetType = findViewById(R.id.xGetJobType);
        PerfectScrollableTextView mGetDescription = findViewById(R.id.xGetJobDesc);
        TextView mGetLocation = findViewById(R.id.xGetJobLocation);
        TextView mGetSalary = findViewById(R.id.xGetJobSalary);
        TextView mGetContact = findViewById(R.id.xGetJobApplyContact);

        mGetJobID.setText(setSpannableString("Job ID: "));
        mGetJobID.append(j.getId());

        mGetJobTitle.setText(setSpannableString("Job Title: "));
        mGetJobTitle.append(j.getTitle());

        mGetPostedDate.setText(setSpannableString("Posted Date: "));
        mGetPostedDate.append(j.getPostDate());

        mGetApplyDate.setText(setSpannableString("Apply By: "));
        mGetApplyDate.append(j.getApplyDate());

        mGetStatus.setText(setSpannableString("Employment Status: "));
        mGetStatus.append(j.getStatus());

        mGetType.setText(setSpannableString("Employment Type: "));
        mGetType.append(j.getType());

        mGetDescription.setText(setSpannableString("Description: "));
        mGetDescription.append(j.getDescription());

        mGetLocation.setText(setSpannableString("City/Location: "));
        mGetLocation.append(j.getLocation());

        mGetSalary.setText(setSpannableString("Salary/Wages: "));
        mGetSalary.append(j.getSalary());

        mGetContact.setText(setSpannableString("To Apply: "));
        mGetContact.append(j.getContact());

    }

    public SpannableString setSpannableString(String textString) {

        SpannableString spanString = new SpannableString(textString);
        spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, textString.length(), 0);
        return spanString;
    }
}
