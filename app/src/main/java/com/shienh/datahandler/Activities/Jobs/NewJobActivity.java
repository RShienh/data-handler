package com.shienh.datahandler.Activities.Jobs;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.shienh.datahandler.Models.Jobs;
import com.shienh.datahandler.R;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Objects;

import es.dmoral.toasty.Toasty;

public class NewJobActivity extends AppCompatActivity {

    private TextInputEditText mAddJobTitle, mJobID, mDescription,
            mLocation, mSalary, mContact;
    private Button mAddJobPostDate, mAddJobEndDate, mJobTypeFull, mJobTypePart,
            mJobStatusOpen, mJobStatusClosed, mSave;
    private String id = "", title = "", status = "", postDate = "", endDate = "",
            type = "", desc = "", location = "", salary = "", contact = "", success = "", issue = "";

    private android.support.v7.widget.CardView mView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_job);

        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
        setTitle("Job Postings");

        mView = findViewById(R.id.xAddNewJobView);
        mView.requestFocus();


        mJobID = findViewById(R.id.xEditJobID);
        mAddJobTitle = findViewById(R.id.xEditJobTitle);
        mAddJobPostDate = findViewById(R.id.xEditJobPostDate);
        mAddJobEndDate = findViewById(R.id.xEditJobEndDate);
        mJobTypeFull = findViewById(R.id.xJobTypeFull);
        mJobTypePart = findViewById(R.id.xJobTypePart);
        mJobStatusOpen = findViewById(R.id.xJobStatusOpen);
        mJobStatusClosed = findViewById(R.id.xJobStatusClosed);
        mDescription = findViewById(R.id.xEditJobDesc);
        mLocation = findViewById(R.id.xEditJobLocation);
        mSalary = findViewById(R.id.xEditJobSalary);
        mContact = findViewById(R.id.xEditJobApplyContact);

        mSave = findViewById(R.id.xJobPostingSave);


        mAddJobPostDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int dayOfMonth = c.get(Calendar.DAY_OF_MONTH);
                c.set(2019,0,1);
                DatePickerDialog datePickerDialog = new DatePickerDialog(NewJobActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Calendar c = Calendar.getInstance();
                        c.set(Calendar.YEAR, year);
                        c.set(Calendar.MONTH, month);
                        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        postDate = DateFormat.getDateInstance(DateFormat.DEFAULT).format(c.getTime());
                        mAddJobPostDate.setText(postDate);
                    }
                }, year, month, dayOfMonth);
                datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
                datePickerDialog.show();
                mAddJobPostDate.setBackgroundResource(R.color.colorAccent);
            }
        });

        mAddJobEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int dayOfMonth = c.get(Calendar.DAY_OF_MONTH);
                c.set(2019,0,1);
                DatePickerDialog datePickerDialog = new DatePickerDialog(NewJobActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Calendar c = Calendar.getInstance();
                        c.set(Calendar.YEAR, year);
                        c.set(Calendar.MONTH, month);
                        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        endDate = DateFormat.getDateInstance(DateFormat.DEFAULT).format(c.getTime());
                        mAddJobEndDate.setText(endDate);
                    }
                }, year, month, dayOfMonth);
                datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
                datePickerDialog.show();
                mAddJobEndDate.setBackgroundResource(R.color.colorAccent);
            }
        });


        mJobTypeFull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = mJobTypeFull.getText().toString().trim();
                mJobTypeFull.setBackgroundResource(R.color.colorAccent);
                mJobTypePart.setBackgroundColor(Color.TRANSPARENT);
                Toasty.info(getApplicationContext(), type+" is selected", Toasty.LENGTH_SHORT).show();
            }
        });

        mJobTypePart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = mJobTypePart.getText().toString().trim();
                mJobTypePart.setBackgroundResource(R.color.colorAccent);
                mJobTypeFull.setBackgroundColor(Color.TRANSPARENT);
                Toasty.info(getApplicationContext(), type+" is selected", Toasty.LENGTH_SHORT).show();
            }
        });

        mJobStatusOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = mJobStatusOpen.getText().toString().trim();
                mJobStatusOpen.setBackgroundResource(R.color.colorAccent);
                mJobStatusClosed.setBackgroundColor(Color.TRANSPARENT);
                Toasty.info(getApplicationContext(), status+" status has been selected", Toasty.LENGTH_SHORT).show();
            }
        });

        mJobStatusClosed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = mJobStatusClosed.getText().toString().trim();
                mJobStatusOpen.setBackgroundColor(Color.TRANSPARENT);
                mJobStatusClosed.setBackgroundResource(R.color.colorAccent);
                Toasty.info(getApplicationContext(), status+" status has been selected", Toasty.LENGTH_SHORT).show();
            }
        });

        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveJobPosting();
                if (!issue.isEmpty()) {
                    Toasty.error(getApplicationContext(), issue, Toasty.LENGTH_LONG).show();
                } else {
                    Toasty.success(getApplicationContext(), success, Toasty.LENGTH_LONG).show();
                }
            }
        });
    }


    private void saveJobPosting() {
        title = Objects.requireNonNull(mAddJobTitle.getText()).toString().trim();
        id = Objects.requireNonNull(mJobID.getText()).toString().trim();
        desc = Objects.requireNonNull(mDescription.getText()).toString().trim();
        location = Objects.requireNonNull(mLocation.getText()).toString().trim();
        salary = Objects.requireNonNull(mSalary.getText()).toString().trim();
        contact = Objects.requireNonNull(mContact.getText()).toString().trim();
        if (id.trim().isEmpty()) {
            issue = "Job ID is empty";
        } else if (title.isEmpty()) {
            issue = "Title is empty";
        } else if (postDate.trim().isEmpty()) {
            issue = "Posted date has not been selected";
        } else if (endDate.trim().isEmpty()) {
            issue = "Apply by date has not been selected";
        } else if (type.trim().isEmpty()) {
            issue = "Type of employment has not been selected";
        } else if (status.trim().isEmpty()) {
            issue = "Status of the job has not been selected";
        } else if (desc.isEmpty()) {
            issue = "Job description is empty";
        } else if (location.isEmpty()) {
            issue = "Job location is empty";
        } else if (salary.isEmpty()) {
            issue = "Salary has not be defined";
        } else if (contact.isEmpty()) {
            issue = "Application contact is empty";
        } else {
            CollectionReference JobRef = FirebaseFirestore.getInstance().collection("Jobs");
            JobRef.add(new Jobs(id, title, postDate, endDate, type, status, desc, location, salary, contact));
            success = "Job has been added";
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            }, 2000);
        }
    }
}
