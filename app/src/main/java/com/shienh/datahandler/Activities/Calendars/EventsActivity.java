package com.shienh.datahandler.Activities.Calendars;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.shienh.datahandler.R;

public class EventsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        Button mLake = findViewById(R.id.xLakeCalendar);
        Button mDuff = findViewById(R.id.xDufferinCalendar);
        Button mChaun = findViewById(R.id.xChaunceyCalendar);

        mLake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EventsActivity.this, LakeShoreCalendarActivity.class));
            }
        });

        mDuff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EventsActivity.this, DufferinCalendarActivity.class));
            }
        });

        mChaun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EventsActivity.this, ChaunceyCalendarActivity.class));
            }
        });
    }
}
