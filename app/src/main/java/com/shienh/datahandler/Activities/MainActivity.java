package com.shienh.datahandler.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.button.MaterialButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.shienh.datahandler.Activities.Calendars.EventsActivity;
import com.shienh.datahandler.Activities.Jobs.JobsActivity;
import com.shienh.datahandler.R;

public class MainActivity extends AppCompatActivity {

    private MaterialButton mAddJobs, mAddEvents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAddJobs = findViewById(R.id.xAddJobMain);
        mAddEvents = findViewById(R.id.xAddEventMain);

        mAddJobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, JobsActivity.class));
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
            }
        });

        mAddEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, EventsActivity.class));
            }
        });
    }
}
