package com.shienh.datahandler.Activities.Calendars;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.shienh.datahandler.Adapters.EventsAdapter;
import com.shienh.datahandler.Models.Events;
import com.shienh.datahandler.R;

public class LakeShoreCalendarActivity extends AppCompatActivity {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private String collectionPath = "LakeEvents";
    private CollectionReference lakeEventRef = db.collection(collectionPath);
    private EventsAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lake_shore_calendar);

        setupRecyclerView();

        FloatingActionButton mAddNewLakeEvent = findViewById(R.id.xAddNewLakeEvent);
        mAddNewLakeEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LakeShoreCalendarActivity.this, NewEventActivity.class);
                i.putExtra("place","Lakeshore");
                i.putExtra("Reference", collectionPath);
                startActivity(i);
            }
        });
    }

    private void setupRecyclerView() {
        Query q = lakeEventRef.orderBy("eventDate", Query.Direction.DESCENDING);
        FirestoreRecyclerOptions<Events> options = new FirestoreRecyclerOptions.Builder<Events>().setQuery(q, Events.class).build();

        mAdapter = new EventsAdapter(options);

        final RecyclerView recyclerView = findViewById(R.id.xLakeEventRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull final RecyclerView.ViewHolder viewHolder, int i) {
                AlertDialog alertDialog = new AlertDialog.Builder(LakeShoreCalendarActivity.this).create();
                alertDialog.setTitle("Delete?");
                alertDialog.setMessage("Do you want to delete this?");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAdapter.deleteItem(viewHolder.getAdapterPosition());
                        recreate();
                    }
                });

                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        recreate();
                    }
                });
                alertDialog.show();
            }
        }).attachToRecyclerView(recyclerView);


    }

    @Override
    protected void onStart() {
        super.onStart();
        mAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAdapter.stopListening();
    }
}
